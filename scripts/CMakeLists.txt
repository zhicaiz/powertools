# add scripts
file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/scripts)
file(GLOB scripts [a-zA-Z]*.sh)
foreach(target ${scripts})
  get_filename_component(scriptname ${target} NAME)
  if(NOT EXISTS ${CMAKE_BINARY_DIR}/scripts/${scriptname})
    execute_process(COMMAND ln -s ${CMAKE_CURRENT_SOURCE_DIR}/${scriptname} ${CMAKE_BINARY_DIR}/scripts)
  endif()
endforeach()

