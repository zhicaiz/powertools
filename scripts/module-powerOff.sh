#!/bin/bash

#
# Disable the power on a X hybrid
#

# HV mux
./bin/pbv3_field write CntSetCHV0en 0
./bin/pbv3_field write CntSetHV0en 0

# Hybrid LDOs
./bin/pbv3_field write CntSetHyLDO0en 0
./bin/pbv3_field write CntSetCHyLDO0en 0
./bin/pbv3_field write CntSetHyLDO1en 0
./bin/pbv3_field write CntSetCHyLDO1en 0
./bin/pbv3_field write CntSetHyLDO2en 0
./bin/pbv3_field write CntSetCHyLDO2en 0

./bin/pbv3_field write CntSetHxLDO0en 0
./bin/pbv3_field write CntSetCHxLDO0en 0
./bin/pbv3_field write CntSetHxLDO1en 0
./bin/pbv3_field write CntSetCHxLDO1en 0
./bin/pbv3_field write CntSetHxLDO2en 0
./bin/pbv3_field write CntSetCHxLDO2en 0

# resetB
./bin/pbv3_field write RstCntHyHCCresetB 0
./bin/pbv3_field write RstCntCHyHCCresetB 0
./bin/pbv3_field write RstCntHxHCCresetB 0
./bin/pbv3_field write RstCntCHxHCCresetB 0


# DCDC
./bin/pbv3_field write DCDCen 0
./bin/pbv3_field write DCDCenC 0
