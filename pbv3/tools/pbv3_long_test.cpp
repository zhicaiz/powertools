#include <unistd.h>
#include <string.h>
#include <getopt.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>

#include <nlohmann/json.hpp>

#include "Logger.h"

#include "AMACv2.h"
#include "EndeavourComException.h"

#include "PBv3TBConf.h"
#include "PBv3TBSingle.h"
#include "PBv3TestTools.h"
#include "PBv3ConfigTools.h"
#include "PBv3Utils.h"

//------ SETTINGS
uint8_t pbNum = 0;
std::string configfile = "config.json";
std::string equipConfigFile = "config/equip_testbench.json";
//---------------

#define PERIOD_LONG 600
#define PERIOD_MONITOR 1
void usage(char* argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] datadir" << std::endl;
  std::cerr << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -b, --board             Powerboard number (default: " << (uint32_t)pbNum << ")" << std::endl;
  std::cerr << " -c, --config            Config for initializing the AMAC. (default: " << configfile << ")" << std::endl;
  std::cerr << " -e, --equip config.json Equipment configuration file (default: " << equipConfigFile << ")" << std::endl;
  std::cerr << " -d, --debug             Enable more verbose printout"  << std::endl;
  std::cerr << std::endl;
  std::cerr << std::endl;
}

int main(int argc, char* argv[])
{
  if (argc < 2)
    {
      usage(argv);
      return 1;
    }

  int c;
  while (1)
    {
      int option_index = 0;
      static struct option long_options[] =
	{
	  {"board"    , required_argument, 0,  'b' },
	  {"config"   , required_argument, 0,  'c' },
	  {"equip"    , required_argument, 0,  'e' },
	  {"debug"    , no_argument      , 0,  'd' },
	  {0          , 0                , 0,  0 }
	};

      c = getopt_long(argc, argv, "b:c:e:d", long_options, &option_index);
      if (c == -1)
	break;

      switch (c)
	{
	case 'b':
	  pbNum = atoi(optarg);
	  break;
	case 'c':
	  configfile = optarg;
	  break;
	case 'e':
	  equipConfigFile = optarg;
	  break;
	case 'd':
	  logIt::incrDebug();
	  break;
	default:
	  std::cerr << "Invalid option supplied. Aborting." << std::endl;
	  std::cerr << std::endl;
	  usage(argv);
	}
    }

  if (argc-optind < 1)
    {
      std::cout << argc << " " << optind << " " << 1 << std::endl;
      std::cerr << "Required paths missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }

  std::string outDir = argv[optind++];

  logger(logDEBUG) << "Settings";
  logger(logDEBUG) << " PB number: " << (uint32_t)pbNum;
  logger(logDEBUG) << " outDir: " << outDir;

  // Output file
  std::string fileName = outDir + "/" + PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) + "_pbv3-test.json";
  logger(logINFO) << "Results stored in " << fileName;
  std::fstream outfile(fileName, std::ios::out);
  if(!outfile.is_open())
    {
      logger(logERROR) << "Unable to create results file " << fileName;
      return 2;
    }

  // Create and initialize the testbench
  PBv3TBConf factory_pbv3tb(equipConfigFile);
  std::shared_ptr<PBv3TB> tb=factory_pbv3tb.getPBv3TB("default");
  if(tb==nullptr)
    return 1;

  // Turn on power
  logger(logINFO) << "Turn on PS fully";
  tb->powerLVOn();
  std::this_thread::sleep_for(std::chrono::milliseconds(500));

  // Init com
  json config;
  if(!configfile.empty())
    {
      std::ifstream fh_in(configfile);
      if(fh_in.is_open())
	fh_in >> config;
    }


  logger(logINFO) << "Init AMAC";
  std::shared_ptr<AMACv2> amac=tb->getPB(pbNum);
  amac->init();
  PBv3ConfigTools::configAMAC(amac, config, false);
  amac->initRegisters();
  PBv3ConfigTools::saveConfigAMAC(amac, config);

  std::shared_ptr<PowerSupplyChannel> lv  =tb->getLVPS();
  std::shared_ptr<PowerSupplyChannel> hv  =tb->getHVPS();

  // 
  // Start running tests in a loop forever!
  //
  //time in minutes when the monitor is written to json files
  double time_write_mon =1.0;

  while(true)
    {
      //
      // Run the long tests

      //Run all calibrations
      json calib_all;// = PBv3ConfigTools::calibrateAll(amac);
      // Prepare the output structure
      json testSum;
      testSum["program"] = argv[0];
      testSum["config"] = config;
      testSum["calib_all"] = calib_all;
      testSum["time"]["start"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 

      // Start testing
      //Run efficiency once to get baseline current (ie powercycle)

      uint32_t test=0;

      try { testSum["tests"][test++] = PBv3TestTools::runBER(amac);  } 
      catch(const EndeavourComException &e) { logger(logERROR) << e.what(); }
      catch(std::string e) {logger(logERROR) << e; testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	outfile << std::setw(4) << testSum << std::endl;
	outfile.close();  break;}


      try { testSum["tests"][test++] = PBv3TestTools::testLvEnable(0, tb);  } 
      catch(const EndeavourComException &e) { logger(logERROR) << e.what(); } 
      catch(std::string e) {logger(logERROR) << e; testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	outfile << std::setw(4) << testSum << std::endl;
	outfile.close(); break;}

      try { testSum["tests"][test++] = PBv3TestTools::testHvEnable(0, tb); } 
      catch(const EndeavourComException &e) { logger(logERROR) << e.what(); } 
      catch(std::string e) {logger(logERROR) << e; testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	outfile << std::setw(4) << testSum << std::endl;
	outfile.close();  break;}

      try { testSum["tests"][test++] = PBv3TestTools::measureHvSense(0, tb);  }
      catch(const EndeavourComException &e) { logger(logERROR) << e.what(); } 
      catch(std::string e) {logger(logERROR) << e;testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	outfile << std::setw(4) << testSum << std::endl;
	outfile.close();  break;}

      try { testSum["tests"][test++] = PBv3TestTools::measureEfficiency(0, tb, 100, 0, 3500); }
      catch(const EndeavourComException &e) { logger(logERROR) << e.what(); } 
      catch(std::string e) {logger(logERROR) << e; testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	outfile << std::setw(4) << testSum << std::endl;
	outfile.close();  break;}

      try { testSum["tests"][test++] = PBv3TestTools::calibrateAMACslope (0, tb, 0.01, false);}
      catch(const EndeavourComException &e) { logger(logERROR) << e.what(); } 
      catch(std::string e) {logger(logERROR) << e; testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	outfile << std::setw(4) << testSum << std::endl;
	outfile.close(); break;}

      try { testSum["tests"][test++] = PBv3TestTools::calibrateAMACoffset(amac,      false); } 
      catch(const EndeavourComException &e) { logger(logERROR) << e.what(); } 
      catch(std::string e) {logger(logERROR) << e;testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	outfile << std::setw(4) << testSum << std::endl;
	outfile.close();  break;}
      // try{  testSum["tests"][test++] = PBv3TestTools::calibrateAMACcm    (amac, 10000);}
      // catch(const EndeavourComException &e) { logger(logERROR) << e.what(); } 
      // catch(std::string e) {logger(logERROR) << e; testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
      //   outfile << std::setw(4) << testSum << std::endl;
      //   outfile.close(); break;}


      testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
      outfile << std::setw(4) << testSum << std::endl;
      outfile.close();

      //Check if 30min has elapsed
      auto t_out = std::chrono::system_clock::now();
      auto t_in = std::chrono::system_clock::now();
      auto t_in2 = std::chrono::system_clock::now();
      std::chrono::duration<double> elapse = t_in-t_out;
      std::chrono::duration<double> elapse2 = t_in2-t_in;

      int iter = 0;
      //
      // Monitor things for a while (30 min explicitly here)

      // Enable load
      tb->setLoad(pbNum,2);      
      tb->loadOn(pbNum);

      amac->wrField(&AMACv2::DCDCen, 1);
      amac->wrField(&AMACv2::DCDCenC, 1);

      // Enable HV
      tb->powerHVOn();

      amac->wrField(&AMACv2::CntSetHV0en, 1);
      amac->wrField(&AMACv2::CntSetCHV0en, 1);

      //for(uint32_t iMon=0;iMon<PERIOD_LONG/PERIOD_MONITOR;iMon++)
      while(elapse.count()/60. < 30.)
	{
	  std::string fileName = outDir + "/" + PBv3Utils::getTimeAsString(std::chrono::system_clock::now()) + "_pbv3-monitor.json";
	  std::fstream outfile(fileName, std::ios::out);

	  // Prepare the output structure
	  json testSum;
	  testSum["calibrate"]; // =  PBv3ConfigTools::calibrateAll(amac);
	  testSum["program"] = argv[0];
	  testSum["time"]["start"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now());

	  // Start testing ( 1 min, then the file is written)
	  while(elapse2.count()/60. < time_write_mon)
	    {
	      try
		{
		  if(std::dynamic_pointer_cast<PBv3TBSingle>(tb))
		    testSum["tests"][iter] = PBv3TestTools::readStatus(amac, lv, std::dynamic_pointer_cast<PBv3TBSingle>(tb)->getLoadPtr(), hv);
		  else
		    testSum["tests"][iter] = PBv3TestTools::readStatus(amac, lv, nullptr, hv);
		}
	      catch(const EndeavourComException &e)
		{ logger(logERROR) << e.what(); }
	      catch(std::string e) {logger(logERROR) << e; 
		testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
		outfile << std::setw(4) << testSum << std::endl;
		outfile.close(); 
		break;}

	      t_in2 = std::chrono::system_clock::now();
	      elapse2 = t_in2-t_in;

	      iter++;
	    }
	  std::cout<<elapse2.count()/60.<<std::endl;

	  iter = 0;

	  testSum["time"]["end"] = PBv3Utils::getTimeAsString(std::chrono::system_clock::now()); 
	  outfile << std::setw(4) << testSum << std::endl;
	  outfile.close();
	  //updating elapsed time
	  t_in = std::chrono::system_clock::now();
	  t_in2 = std::chrono::system_clock::now();
	  elapse = t_in-t_out;
	  elapse2 = t_in2-t_in;
	}

      amac->wrField(&AMACv2::DCDCen, 0);
      amac->wrField(&AMACv2::DCDCenC, 0);
      tb->loadOff(pbNum);

      amac->wrField(&AMACv2::CntSetHV0en, 0);
      amac->wrField(&AMACv2::CntSetCHV0en, 0);
      tb->powerHVOff();

    }

  //
  // Power off
  tb->powerLVOff();
  tb->powerHVOff();

  return 0;
}
