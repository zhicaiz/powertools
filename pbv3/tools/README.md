# The powertools Tools

The powertools package comes with several programs that can be used to control a Powerboard either on a test setup or a module. The tools are divided into two categories: general tools and tools used during Powerboard QC. The description of the tools follows. Use the `--help` argument for the actual usage.

All tools take the following common optional arguments:
- `-b n` Powerboard index in the testbench.
- `-e config.json` Path to hardware configuration file defining the testbench and (optinal) power supplies.
- `-d` Increase debug output level.

## General Tools

### endeavour
General communication with an AMAC using the endeavour protocol. The SETID, READ and WRITE commands are implemented.

### EndeavourBER
Communication test with a Powerboard by write/reading a specific register many times.

### pbv3_config
Load the AMACv2 configuration file with register values into an AMAC. 

### pbv3_field
Reading and writing of fields inside AMAC. Fields are regions of the register that correspond to a specific setting. Run `pbv3_field list` to see a list of all available fields.

### pbv3_print_status
Prints the status of a Powerboard in a nicely formatted table.

### pbv3_monitor
Constantly monitor the Powerboard with calibration procedures interleaved every few minutes.

### pbv3_powerOff
Turn off the power to a testbench. Both low and high voltages.

### pbv3_readADC
Read the ADC counts from an AM block. Run `pbv3_readADC --help` to see a list of AM block names.

### pbv3_setcal
Set the CAL value for all Powerboards.

### pbv3_setofin
Set the OFin value for a Powerboard.

### pbv3_tune
Perform the tuning of a Powerboard and apply/save the resulting configuration. See the `pbv3_config` command for the format of the saved documentation.

Specific components can be tuned on their own (see `pbv3_tune --help` for options), but a complete `all` tuning does the following:

1. Setting AMbg to 600mV
2. Setting VDDreg to 1.2V
3. Tuning the ramp gain to 1mV/count
4. AM calibration (slope and offset)
5. NTC reference calibration
6. PTAT/CTAT calibration
7. current monitor calibration

## QC Tools
### pbv3_active_fancy_load
Hacky tool to run the DC/DC efficiency scan using an external load with the mass test system.

### pbv3_basic_test
Run the QC test on a single Powerboard. Works with both the mass test system and a single test board.

### pbv3_long_test
Repeats the QC tests at specified intervals.

### pbv3_mass_test
Run the QC test suite using the mass test system on multiple Powerboards loaded on a carrier card.

### pbv3_readMux
Read the output voltage of a Powerboard. These are multiplexed on the mass tester card.

### pbv3_setload
Set variable load to a certain value.

### pbv3_whatisalive
Quick check of what Powerboards are alive (good libPOL12V output voltage).

### pbv3_active_calibrate
Calibrates the variable board on an active board using an external power supply connected to it instead of a load. The power supply that will provide the 1.5V needs to be defined as the `FakePB` channel in the hardware configuration file.

### pbv3_active_init
Initialize all the devices to desired default values on the active board. Needs to be called before powering on the 11V LV supplied to a carrier card.

The test programs (`pbv3_basic_test` and `pbv3_mass_test`) do the initialization themselves. This tool is only necessary when running custom tests outside of the main QC programs.
