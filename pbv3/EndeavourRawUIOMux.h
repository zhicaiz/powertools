#ifndef ENDEAVOURRAWUIOMUX_H
#define ENDEAVOURRAWUIOMUX_H

#include <memory>

#include "EndeavourRawUIO.h"

#include "IOExpander.h"

//! \brief AMAC communication via UIO connected to endeavour_master and multiplexed
/**
 * For use with cases where the `endeavour_master` CMDin/out is multiplexed to 
 * several Powerboards.
 *
 * Works by setting the multiplexer select code to the specified Powerboard. The
 * signal inversion is specified at the same time.
 *
 * The DIT/DAH lenghts are set globally.
 */
class EndeavourRawUIOMux : public EndeavourRawUIO
{
public:
  EndeavourRawUIOMux(uint8_t code                                     , std::shared_ptr<IOExpander> sel, std::shared_ptr<DeviceCom> fpgaCom);
  EndeavourRawUIOMux(uint8_t code, bool invertCMDin, bool invertCMDout, std::shared_ptr<IOExpander> sel, std::shared_ptr<DeviceCom> fpgaCom);

  void sendData(unsigned long long int data, unsigned int size);

private:
  uint8_t m_code;
  bool m_invertCMDin;
  bool m_invertCMDout;

  std::shared_ptr<IOExpander> m_sel;
};

#endif //ENDEAVOURRAWUIOMUX_H
